<div class="col events-col <?php the_sub_field('events_width'); ?>">
	<h3><?php the_sub_field('events_title'); ?></h3>
	<?php $term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		if($term) { 
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount, 
			'tax_query' => array(
				array(
					'taxonomy' => 'tribe_events_cat',
					'terms' => array($term->term_id)
				)
			)
		) ); 
		} else {
			$events_query = new WP_Query( array( 'post_type' => 'tribe_events', 'showposts' => $amount ) );
		}
		?>
	<ol <?php if(get_sub_field('show_categories') == "yes") { ?> class="categories" <?php } ?>>
		<?php if ($events_query->have_posts()) : while ($events_query->have_posts()) : $events_query->the_post(); ?>
		<li class="tribe-events-list-widget-events">
			<?php 
				if(get_sub_field('events_width') == "two") {
					if(get_sub_field('show_image') == "yes") {
						if ( has_post_thumbnail() ) {
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
							$url = $thumb['0']; ?>
							<a href="<?php the_permalink() ?>"><img src="<?=$url?>" alt="<?php the_title(); ?>" /></a>
						<?php } else { ?>
					        <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" /></a>
					<?php }
					}
				} 
			?>
			<div class="item">
				<?php do_action( 'tribe_events_list_widget_before_the_event_title' ); ?>
				<!-- Event Title -->
				<?php
					if(get_sub_field('show_categories') == "yes") {
						$event_cat = wp_get_object_terms( $post->ID, 'tribe_events_cat' );
						if ( ! empty( $event_cat ) ) {
							if ( ! is_wp_error( $event_cat ) ) {
								echo '<span class="category">';
									foreach( $event_cat as $term ) {
										echo '<a href="' . get_term_link( $term->slug, 'tribe_events_cat' ) . '">' . esc_html( $term->name ) . '</a>'; 
									}
								echo '</span>';
							}
						}
					}
				?>					
				<a href="<?php the_permalink() ?>">
					<h4><?php the_title(); ?></h4>
					<?php do_action( 'tribe_events_list_widget_after_the_event_title' ); ?>
					<!-- Event Time -->
					<?php do_action( 'tribe_events_list_widget_before_the_meta' ) ?>
					<div class="duration">
						<?php echo tribe_events_event_schedule_details(); ?>
					</div>
					<?php do_action( 'tribe_events_list_widget_after_the_meta' ) ?>
				</a>
			</div>
		</li>
		<?php endwhile; ?>
	</ol>
	<?php else : ?>
	<p>There are no upcoming events. Please check back soon.</p>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<?php if( $term ) { ?>
	<a class="btn" href="/events/category/<?php echo $term->slug; ?>/">View All<span class="hidden"> <?php echo $term->name; ?></span></a>
	<?php } else { ?>
	<a class="btn" href="/events/">View All<span class="hidden"> Events</span></a>
	<?php } ?>
</div>