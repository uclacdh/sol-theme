<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<h1 class="search-title">
						<span><?php _e( 'Search Results for:', 'bonestheme' ); ?></span> <?php echo esc_attr(get_search_query()); ?>
					</h1>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
							<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
							<section class="entry-content cf">
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink() ?>" class="btn">Read More</a>
							</section>
						</article>
						
					<?php endwhile; ?>
					<?php bones_page_navi(); ?>
					<?php else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h3><?php _e( 'Sorry, No Results.', 'bonestheme' ); ?></h3>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Try your search again.', 'bonestheme' ); ?></p>
							</section>
						</article>

					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
